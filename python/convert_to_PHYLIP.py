from itertools import repeat
from os.path import expanduser, join
home = join(expanduser("~"),"src_R/ELITE_qtl_mapping/output")
file_in = open(join(home, 'sdps.random.for.trees.txt'), 'r')
file_out = open(join(home, 'sdps.random.for.trees.phylip.txt'), 'w')

transposed_file = {1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "F", 7: "G", 8: "H"}

for key in transposed_file:
    transposed_file[key] = transposed_file[key] + "        "

col = 1
for line in file_in:
    for i in range(1,8):
        transposed_file[i] = transposed_file[i] + line[i]
    col = col + 1
    if col == 11:
        for i in range(1, 8):
            transposed_file[i] = transposed_file[i] + " "
        col = 1

for i in range(1,8):
    file_out.write(transposed_file[i])
    file_out.write("\n")
