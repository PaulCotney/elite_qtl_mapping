#!/bin/bash
module load r
cd /pine/scr/p/a/paulcotn/edit_distance_dev/slurm_runs
i=19
for i in {1..19}; do
sbatch --job-name=edist_$1 -o "edist_$i".out -p general --mem 100G -n 1 -t 1-00:00:00 --wrap "Rscript /pine/scr/p/a/paulcotn/edit_distance_dev/scripts/bulk_edit_distance.R "'"'"$i"'"'" "'"'"/pine/scr/p/a/paulcotn/edit_distance_dev/data/"'"'" "'"'"/pine/scr/p/a/paulcotn/edit_distance_dev/output/"'"'" "
done
i=X
sbatch --job-name=edist_$1 -o "edist_$i".out -p general --mem 100G -n 1 -t 1-00:00:00 --wrap "Rscript /pine/scr/p/a/paulcotn/edit_distance_dev/scripts/bulk_edit_distance.R "'"'"$i"'"'" "'"'"/pine/scr/p/a/paulcotn/edit_distance_dev/data/"'"'" "'"'"/pine/scr/p/a/paulcotn/edit_distance_dev/output/"'"'" "