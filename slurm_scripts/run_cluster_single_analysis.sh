#!/bin/bash
module load r
HOME_DIR=/pine/scr/p/a/paulcotn/ELITE_mapping
mkdir -p $HOME_DIR/plots/output
cd $HOME_DIR/slurm_runs
for i in {1..50}; do sbatch -o "build_outlier_info_cluster_single_$i".out --job-name "build_outlier_info_cluster_single" -p general --mem 10G -n 1 -t 2-00:00:00 --wrap "Rscript /pine/scr/p/a/paulcotn/ELITE_mapping/scripts/build_single_outlier_info_cluster.R "'"'"$i"'"'" "'"'"/pine/scr/p/a/paulcotn/cluster_analysis/data/probes.genotype.details.rds"'"'"  "'"'"/pine/scr/p/a/paulcotn/ELITE_mapping/output/"'"'" "; done