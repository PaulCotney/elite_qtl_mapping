## Rules for merge of TEis between 200bp - 550bp

Attribute | First TEi | Second TEi | Action |
 ---- | ---- | ---- | ---- |
B6 | T | T |  |
Strand | + | + |  |
side | Start | Start | ? |
side | Start | End | ? |
side | End | Start | ? |
side | End | End | ? |

Attribute | First TEi | Second TEi | Action |
 ---- | ---- | ---- | ---- |
B6 | T | T |  |
Strand | + | - |  |
side | Start | Start | FPMV |
side | Start | End | Anwica |
side | End | Start | Awnica |
side | End | End | Keep Both |

Attribute | First TEi | Second TEi | Action |
 ---- | ---- | ---- | ---- |
B6 | T | T |  |
Strand | - | - |  |
side | Start | Start | Keep Both |
side | Start | End | Anwica |
side | End | Start | Awnica |
side | End | End | FPMV |



Attribute | First TEi | Second TEi | Action |
 ---- | ---- | ---- | ---- |
B6 | T | F |  |
Strand | + | - |  |
side | Start | Any | Anwica |
side | End | Any | Keep Both |


Attribute | First TEi | Second TEi | Action |
 ---- | ---- | ---- | ---- |
B6 | T | F |  |
Strand | - | + |  |
side | Start | Any | Keep Both |
side | End | Any | Anwica |

Attribute | First TEi | Second TEi | Action |
 ---- | ---- | ---- | ---- |
B6 | F | F |  |
strand | any | any | |
side | Any | Any | Keep Both |