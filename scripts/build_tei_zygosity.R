
# Builds the matrix of calls (1,H, N, 0) for each TEi derived from the probes that
# are actually not filtered out.
library(conflicted)
suppressPackageStartupMessages(library(tidyverse))
filter <- dplyr::filter
lag <- dplyr::lag

param.dir.home <- getwd()
param.dir.data <- file.path(param.dir.home,'data')
param.dir.tmp <- file.path(param.dir.home,'tmp')
param.dir.output <- file.path(param.dir.home,"output")
param.dir.bulk <- file.path(param.dir.data,"bulk")

probe.meta <- readRDS(file.path(param.dir.tmp, "ELITE_probe_table_meta.rds"))

compute_zygosity <- function(X) {
  probe.genotype <- X[!is.na(X$final.value),]
  probe.genotype[probe.genotype$final.value>0,]$final.value = 1
  wide <- probe.genotype %>% pivot_wider(id_cols = c("probe.id"), names_from = sample, values_from = final.value)
  wide <- right_join(probe.meta[,c('TEi.id','probe.id', 'TE')], wide, by = c("probe.id")) 
  zygosity.TE <- wide %>% filter(TE == 1) %>% group_by(TEi.id)  %>% 
    summarise(across(-1, function(x) if (max(x) == 0) {0} else {1})) %>% 
    select(-TE)
  zygosity.ABS <- wide %>% filter(TE == 0) %>% group_by(TEi.id) %>%  
    summarise(across(-1, function(x) if (max(x) == 0) {0} else {2})) %>%
    select(-TE)
  zygosity <- rbind(zygosity.TE, zygosity.ABS) %>% arrange(TEi.id)
  zygosity <- zygosity %>% group_by(TEi.id) %>% summarise_all(sum)
  zygosity[,-1] <- apply(zygosity[,-1],c(1,2), function(x) if (x==3) {'H'} else if (x==2) {'0'} else if (x==1) {'1'} else {'N'} )
  zygosity
}

probe.filter.status <- readRDS(file.path(param.dir.tmp,"probe.annotations.rds")) %>% select(probe.id, remove)
probe.filter.status <- probe.filter.status[probe.filter.status$remove == TRUE,]

probe.cc.details <- readRDS(file.path(param.dir.bulk, "probe_genotype_details.rds"))
probe.cc.details <- probe.cc.details[probe.cc.details$sample != 'CC049M214_UNC_UNC',]
probe.cc.details <- probe.cc.details[!(probe.cc.details$probe.id %in% probe.filter.status$probe.id),]
TEi.cc <- compute_zygosity(probe.cc.details)
saveRDS(TEi.cc, file.path(param.dir.tmp, 'TE_CC_zygosity.rds' ))

probe.sanger.details <- readRDS(file.path(param.dir.tmp, 'probe_sanger_founder_details.rds'))
probe.sanger.details <- probe.sanger.details[!(probe.sanger.details$probe.id %in% probe.filter.status$probe.id),]
TEi.sanger <- compute_zygosity(probe.sanger.details)
TEi.sanger <- TEi.sanger %>% unite("SDP", -1, sep = "", remove = FALSE)
TEi.sanger$SDP <- paste('A', TEi.sanger$SDP, sep ="" )
saveRDS(TEi.sanger, file.path(param.dir.tmp, 'TE_sanger_zygosity.rds' ))

probe.unc.details <- readRDS(file.path(param.dir.tmp, 'probe_unc_founder_details.rds'))
probe.unc.details <- probe.unc.details[!(probe.unc.details$probe.id %in% probe.filter.status$probe.id),]
TEi.unc <- compute_zygosity(probe.unc.details)
TEi.unc <- TEi.unc %>% unite("SDP", -1, sep = "", remove = FALSE)
TEi.unc$SDP <- paste('A', TEi.unc$SDP, sep ="" )
saveRDS(TEi.unc, file.path(param.dir.tmp, 'TE_unc_zygosity.rds' ))
