#!/bin/bash

BASE_DIR="$HOME/Documents/src_R/ELITE_qtl_mapping"
HOME_DIR="$BASE_DIR/scripts"
EXTERNAL_DIR="$BASE_DIR/data/External"
ANNOTATION_DIR="$BASE_DIR/data/annotations"
TMP_DIR="$BASE_DIR/tmp/"
mkdir -p "$TMP_DIR"
cp "$EXTERNAL_DIR"/mouse.genome.csv "$TMP_DIR"/
cp "$EXTERNAL_DIR"/snp_info.csv "$TMP_DIR"/
cp "$EXTERNAL_DIR"/subspecies.csv "$TMP_DIR"/

set -x	
Rscript "$HOME_DIR"/load_ELITE_base.R
Rscript "$HOME_DIR"/normalize_founder_reads.R
Rscript "$HOME_DIR"/build_probe_sdp_founder.R
Rscript "$HOME_DIR"/build_probe_descriptions.R
Rscript "$HOME_DIR"/build_probe_level_sdps.R
Rscript "$HOME_DIR"/build_annotations_files.R
Rscript "$HOME_DIR"/build_sdps_probe.R
Rscript "$HOME_DIR"/build_tei_sdps.R
Rscript "$HOME_DIR"/build_tei_zygosity.R
Rscript "$HOME_DIR"/build_new_category.R
Rscript "$HOME_DIR"/build_db.R
Rscript "$HOME_DIR"/simple_report.R
Rscript "$HOME_DIR"/additional_tables.R
#Rscript "$HOME_DIR"/post_run_sanity_checks.R
#Rscript "$HOME_DIR"/post_run_sanity_distance.R
set +x	

