# simple_report ----
## Description:
##.  Basic report (probe_report.csv / tei_report.csv)
## Author: Paul A Cotney
##
## Date Created: 2024-04-16
##
## Notes:
##
##

## Library ----
library(conflicted)
suppressPackageStartupMessages(library(tidyverse))
filter <- dplyr::filter
setdiff <- dplyr::setdiff
lag <- dplyr::lag

## Load directories ----
param.dir.home <- getwd()
param.dir.config <- file.path(param.dir.home, 'config')
param.dir.data <- file.path(param.dir.home,'data')
param.dir.output <- file.path(param.dir.home,"output")
param.dir.database <- file.path(param.dir.data, "database")

## Includes ----
source(file.path(param.dir.home,'scripts/include_reporting_functions.R'))

## Generate and write the ERVi reports ----

### Write ERV level report ----
write_TEi_report <- function(TEi.db, tei.col.order) {
  print(sprintf("Removing these columns in the TEi report: %s", setdiff(names(TEi.db), tei.col.order$col)))
  print(sprintf("These columns are missing from the DB and should be in there: %s", setdiff(tei.col.order$col, names(TEi.db))))
  TEi.db <- TEi.db[,tei.col.order$col.name]
  TEi.db <- TEi.db %>% arrange(chr, pos, ERVi.id)
}

### Load ERV DBs ----
TEi.db <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))

### Write ERVi report for uploading to dataverse ----

#### Seperate them into those filtered out and not. ----
filtered.ERVi.report <- TEi.db[TEi.db$filtered.out,]
ERVi.report.d.verse <- TEi.db[!TEi.db$filtered.out,]

#### Write out dataverse version ----
ERVi.report.d.verse <- write_TEi_report(ERVi.report.d.verse, read.csv(file.path(param.dir.config,'erv_col_order_dataverse.csv'), stringsAsFactors = FALSE))
write.csv(ERVi.report.d.verse, file.path(param.dir.output, "ERVi.csv"), row.names = FALSE, na = "")

#### Just write out the filtered ERVi in standard format----
filtered.ERVi.report <- write_TEi_report(filtered.ERVi.report, read.csv(file.path(param.dir.config,'erv_col_order.csv'), stringsAsFactors = FALSE))
write.csv(filtered.ERVi.report, file.path(param.dir.output, "ERVi_filtered.csv"), row.names = FALSE, na = "")

### Write ERVi report for standard ERV report
#### Convert boolean blanks to blank ----
b.cols <- c('dom.correct.flag', 'cast.correct.flag', 'mus.correct.flag', 'all.NA.flag',
            'NA.flag', 'missing.dom.flag', 'missing.cast.flag', 'missing.mus.flag', "filtered.out",
            'is.SDP.complete', 'is.cnv', 'is.cnv.ERV.start', 'is.cnv.ERV.end', 'is.cnv.ERV', 'is.cnv.ABS')
TEi.db[,b.cols][!TEi.db[,b.cols]] <- ""

#### Clean up columns ----
tei.report <- write_TEi_report(TEi.db, read.csv(file.path(param.dir.config,'erv_col_order.csv'), stringsAsFactors = FALSE))
#### Save to DB ----
saveRDS(tei.report, file.path(param.dir.database, "ERVi_report.rds"))
#### Write out file ----
write.csv(tei.report, file.path(param.dir.output, "ERVi_report.csv"), row.names = FALSE, na = "")


## Generate and write the probe reports ----

### Load probe database
probe.db <- readRDS(file.path(param.dir.database, "probe_database.rds"))

### Rounding in probe.report ----
n.cols <- c('ELITE.lod', 'lod', 'delta.LOD')
probe.db[,n.cols] <- apply(probe.db[,n.cols],c(1,2),round,2)


### Write standard probe level report ----
write_probe_report <- function(probe.db, probe.col.order) {
  print(sprintf("Removing these columns in the probe report: %s", setdiff(names(probe.db), probe.col.order$col)))
  print(sprintf("These columns are missing from the DB and should be in there: %s", setdiff(probe.col.order$col, names(probe.db))))
  probe.db <- probe.db[,probe.col.order$col] %>% arrange(ELITE.chr, ELITE.pos, ERVi.id, probe.id)
}
probe.report <- write_probe_report(probe.db, read.csv(file.path(param.dir.config,'probe_col_order.csv'), stringsAsFactors = FALSE))
saveRDS(probe.report, file = file.path(param.dir.database, "probe_report.rds"))
write.csv(probe.report, file.path(param.dir.output, "probe_report.csv"), row.names = FALSE, na = "")

### Write probe report for uploading to dataverse ----
#### Convert boolean blanks to FALSE ----
b.cols <- c('probe.is.CNV', 'not.mappable', 'filtered.out')
probe.report[,b.cols][is.na(probe.report[,b.cols])] <- FALSE

#### Seperate them into those filtered out and not. ----
filtered.probe.report <- probe.report[probe.report$filtered.out,]
probe.report.d.verse <- probe.report[!probe.report$filtered.out,]

#### Clean up columns ----
probe.report.d.verse <- write_probe_report(probe.report.d.verse, read.csv(file.path(param.dir.config,'probe_col_order_dataverse.csv'), stringsAsFactors = FALSE))
write.csv(probe.report.d.verse, file.path(param.dir.output, "probe.csv"), row.names = FALSE, na = "")

#### Just write out the filtered probes in standard format ----
filtered.probe.report <- write_probe_report(filtered.probe.report, read.csv(file.path(param.dir.config,'probe_col_order.csv'), stringsAsFactors = FALSE))
write.csv(filtered.probe.report, file.path(param.dir.output, "probe_filtered.csv"), row.names = FALSE, na = "")

