# additional_tables ----
## Description:
##
## Author: Paul A Cotney
##
## Date Created: 2023-05-09
##
## Notes:
## Output plus supporting summary tables
##

## Library ----
library(conflicted)
suppressPackageStartupMessages(library(tidyverse))
filter <- dplyr::filter
setdiff <- dplyr::setdiff
lag <- dplyr::lag

## Load directories ----
param.dir.home <- getwd()
param.dir.data <- file.path(param.dir.home,'data')
param.dir.output <- file.path(param.dir.home,"output")
param.dir.external <- file.path(param.dir.data,"External")
param.dir.database <- file.path(param.dir.data, "database")

## Include external functions ----
source(file.path(param.dir.home,'scripts/include_reporting_functions.R'))

## Sort orders ----
genetic.context.order <- c("intergenic", "intron", "promoter","exon")
samples.used <- unlist(read.csv(file.path(param.dir.external, "sample_names_used.csv")))
merge.types <- c('IAPEY3C_RLTR1IAP', 'MTA_RLTR10', 'MTA_RLTR1IAP', 'IAPEY3C_MTA',
                 'MTA_RLTRETN', 'ERVB7_MTA', 'MERVL_RLTR1IAP', 'MERVL_RLTRETN',
                 'ERVB4_IAPEY3C', 'ERVB4_MERVL', 'ERVB4_MTA', 'ERVB7_RLTR10',
                 'IAPEY3C_MERVL', 'MERVL_RLTR10', 'RLTR10_RLTRETN', 'RLTR1IAP_RLTRETN')
my.id.order.report <- c('MTA','RLTR1IAP','MERVL','RLTR10','IAPEY3C','ERVB7','RLTRETN','ERVB4',
                        'IAPEY3C_RLTR10','RLTR4','MERVL_MTA','ERVB7_RLTRETN','RLTR10_RLTR1IAP','Hybrid*')
fixed.col.names <- c("Fixed mapped", "Fixed unmapped")
seg.col.names <- c("Segregating complete", "Segregating incomplete")
USS.col.names <- c("Unknown segregation status")
de.novo.col.names <- c( "de novo germline in CC", "de novo germline in CC and founders")
table2.3.row.order <- c('RLTR1IAP', 'MERVL', 'MTA', 'RLTR10', 'ERVB7', 'RLTRETN',
                        'IAPEY3C_RLTR10', 'ERVB4', 'RLTR4', 'IAPEY3C', 'MERVL_MTA',
                        'RLTR10_RLTR1IAP', 'ERVB7_RLTRETN', 'Hybrid*', "Total")
type.order <- c('Fixed', 'USS', 'Segregating', 'de novo')
table2.col.order <- c("ERV.type", type.order, "Total", "Relative Activity")
table4.col.order <- c("genomic.context", "subtype", type.order, "Total", "relative.risk")

## Table 2 ERV classified by type and segregation status in the CC population ----
### Read table and exclude filtered and somatics ----
TEi.report <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))[,c('ERV.type', 'segregation.status', 'filtered.out')]
TEi.report <- TEi.report[!TEi.report$filtered.out, ]
TEi.report <- TEi.report[TEi.report$segregation.status != 'de novo somatic',]
### Merge individual seg status segregation.statuss into larger groups ----
TEi.report[TEi.report$segregation.status %in% fixed.col.names, ]$segregation.status <- "Fixed"
TEi.report[TEi.report$segregation.status %in% seg.col.names, ]$segregation.status <- "Segregating"
TEi.report[TEi.report$segregation.status %in% USS.col.names, ]$segregation.status <- "USS"
TEi.report[TEi.report$segregation.status %in% de.novo.col.names, ]$segregation.status <- "de novo"
### Pivot with counts ----
table02<- TEi.report %>% group_by(ERV.type, segregation.status) %>%
  count %>%
  pivot_wider(id_cols = ERV.type, names_from = segregation.status, values_from = n )
rm(TEi.report)
table02[is.na(table02)] <- 0

### Collapse merged types into Hybrid* ----
merged.type.details <- table02[table02$ERV.type %in% merge.types,]
merged.type.details <- merged.type.details %>%
  ungroup() %>%
  bind_rows(summarize(., ERV.type = "Hybrid*", across(where(is.numeric), sum))) %>%
  filter(ERV.type == "Hybrid*")
table02 <- table02[!(table02$ERV.type %in% merge.types),]
table02 <- rbind(table02, merged.type.details)
rm(merged.type.details)

### Add in the totals for the rows ----
table02 <- table02 %>% ungroup() %>% mutate(Total = rowSums(across(where(is.numeric)))) %>% arrange(-Total)
table02[is.na(table02)] <- 0

### Add in relative activity field ----
table02$`Relative Activity` <- round(table02$`de novo` / table02$Total,4)

### Order the rows and columns ----
table02.hybrid <- table02[table02$ERV.type == 'Hybrid*',]
table02 <- table02[table02$ERV.type != 'Hybrid*',] %>% arrange(desc(Total))
table02 <- rbind(table02, table02.hybrid)
table02 <- table02 %>% bind_rows(summarize(., ERV.type = "Total", across(where(is.numeric), sum)))
table02 <- table02[,table2.col.order]
### Remove the sum for relative activity
table02[table02$ERV.type=="Total", "Relative Activity"] <- NA
### Change to text to avoid scientific notation in csv
table02 <- apply(table02,c(1,2), as.character)
### Write the table02 to disk ----
write.csv(table02, file.path(param.dir.output, "table02.csv"), row.names = FALSE, na = "")
rm(list = c("table02", "table02.hybrid"))

## Table 3 Founder strain dependent frequency of ERV segregating in the CC ----

### Create Individual columns from SDP ----
TEi.report <- readRDS(file.path(param.dir.database, "ERVi_database.rds")) %>%
  filter(segregation.status == "Segregating complete" & !filtered.out)
TEi.report <- TEi.report[str_detect(TEi.report$segregation.status,"egregating"), c("ERVi.id","segregation.status", "ERV.type","pop2.sdp")]
TEi.report <- TEi.report %>%  separate( col = pop2.sdp, sep = "", into = c("X1","X2","A","B","C","D","E","F","G","H"), remove = FALSE) %>% select(-X1,-X2)
TEi.report[is.na(TEi.report) | TEi.report == 'N'] <- 0
TEi.report[,LETTERS[1:8]] <- apply(TEi.report[,LETTERS[1:8]],c(1,2),as.integer)

### Create a report from totals for the seg. complete count and the totals for the founders ----
report.right <- TEi.report %>% select(ERV.type,A:H) %>% group_by(ERV.type) %>% summarise(across(where(is.numeric), sum))
report.left <- TEi.report%>% group_by(ERV.type) %>% count(name = "segregating complete")
report.all <- left_join(report.left, report.right, by = "ERV.type")
report.all <- report.all %>% arrange(desc(`segregating complete`))
### Make the hybrid line and remove rows that make up the hybrid and finally add totals ----
hybrid.line <- report.all[report.all$ERV.type %in% merge.types,] %>% ungroup() %>%
  bind_rows(summarize(., ERV.type = "Hybrid*", across(where(is.numeric), sum))) %>%
  filter(ERV.type == "Hybrid*")
table03_compl <- rbind(report.all[!(report.all$ERV.type %in% merge.types),], hybrid.line)
table03_compl <- table03_compl %>%  ungroup() %>%
  bind_rows(summarize(., ERV.type = "Total", across(where(is.numeric), sum)))
### Write the table 3 file to disk ----
write.csv(table03_compl, file.path(param.dir.output, "table03.csv"), row.names = FALSE)

## Table 4 ERV insertions by genomic context and segregation status in the CC population ----
TEi.report <- readRDS(file.path(param.dir.database, "ERVi_database.rds")) %>%
  filter(segregation.status != "de novo somatic")
TEi.report <- TEi.report[!TEi.report$filtered.out, ]
TEi.report$subtype <- ''
TEi.report[TEi.report$genomic.context.derived=="exon" & str_detect(TEi.report$genetic.context.detail, "coding"),]$subtype <-'coding'
TEi.report[TEi.report$genomic.context.derived=="exon" & !str_detect(TEi.report$genetic.context.detail, "coding"),]$subtype <-'non-coding'
TEi.report <- TEi.report[,c("ERVi.id","subtype", "segregation.status", "ERV.type","genomic.context.derived")]

### Merge individual seg status segregation.statuss into larger groups ----
TEi.report[TEi.report$segregation.status %in% fixed.col.names, ]$segregation.status <- "Fixed"
TEi.report[TEi.report$segregation.status %in% seg.col.names, ]$segregation.status <- "Segregating"
TEi.report[TEi.report$segregation.status %in% USS.col.names, ]$segregation.status <- "USS"
TEi.report[TEi.report$segregation.status %in% de.novo.col.names, ]$segregation.status <- "de novo"

TEi.report <- TEi.report %>% group_by(genomic.context.derived, segregation.status, subtype) %>% count()
TEi.report <- TEi.report %>% pivot_wider(names_from = segregation.status, values_from = n, values_fill = 0)

TEi.report$genomic.context.derived <- factor(TEi.report$genomic.context.derived, genetic.context.order)
TEi.report$subtype <- factor(TEi.report$subtype, c('','non-coding','coding'))
TEi.report <- TEi.report[, c("genomic.context.derived","subtype", type.order)] %>% arrange(genomic.context.derived)

#### Row Totals / Column totals ----
names(TEi.report)[1] <- "genomic.context"
TEi.report <- TEi.report %>% ungroup() %>% mutate_if(is.numeric , replace_na, replace = 0) %>%
  mutate(Total = rowSums(across(where(is.numeric)))) %>%
  bind_rows(summarize(., genomic.context = "Total", across(where(is.numeric), sum)))

#### Add in subtotal for the de novos ----
de.novo.rows <- TEi.report %>% filter(genomic.context == "exon") %>%
  bind_rows(summarize(.,genomic.context = "", subtype = "Exon", across(where(is.numeric), sum)))
TEi.report <- rbind(TEi.report, de.novo.rows %>% filter(subtype == "Exon"))
TEi.report <- TEi.report[c(1:3,7,4:6),]

#### Relative Risk ----
TEi.report$rate.de.novo <- NA
TEi.report[TEi.report$genomic.context != "Total", ]$rate.de.novo <-
  TEi.report[TEi.report$genomic.context != "Total", ]$`de novo` /
  TEi.report[TEi.report$genomic.context == "Total", ]$`de novo`
TEi.report$rate.non.de.novo <- NA
TEi.report[TEi.report$genomic.context != "Total", ]$rate.non.de.novo <-
  ( TEi.report[TEi.report$genomic.context != "Total", ]$Total -
      TEi.report[TEi.report$genomic.context != "Total", ]$`de novo` ) /
  ( TEi.report[TEi.report$genomic.context == "Total", ]$Total -
      TEi.report[TEi.report$genomic.context == "Total", ]$`de novo` )
TEi.report <- TEi.report %>% mutate(relative.risk = round(rate.non.de.novo / rate.de.novo,2), .keep="unused")

#### Column order ----
TEi.report <- TEi.report[,table4.col.order]

### Write the table 4 file to disk ----
write.csv(TEi.report, file.path(param.dir.output,"table04.csv"), row.names = FALSE)
