library(conflicted)
suppressPackageStartupMessages(library(tidyverse))
filter <- dplyr::filter
lag <- dplyr::lag
library(Biostrings)

param.dir.home <- getwd()
param.dir.data <- file.path(param.dir.home,"data")
param.dir.database <- file.path(file.path(param.dir.data, "database"))
param.dir.annotations <- file.path(param.dir.data,"annotations")
param.dir.merge <- file.path(param.dir.data,"merge")
param.dir.tmp <- file.path(param.dir.home,'tmp')
param.dir.output <- file.path(param.dir.home,'output')
param.dir.config <- file.path(param.dir.home, 'config')
dir.output.merge <- file.path(param.dir.output, 'merge_script')
param.dir.analysis <- file.path(param.dir.data, "analysis")
param.dir.bulk <- file.path(param.dir.data,'Bulk')

source(file.path(param.dir.home,'scripts/include_reporting_functions.R'))
source(file.path(param.dir.home,'include/misc_functions.R'))


tei.probe.lookup <- readRDS(file.path(param.dir.database, "probe_database.rds"))[,c("TEi.id","probe.id")]
TEi.pairs <- readRDS(file.path(param.dir.tmp, "TEi_pairs.rds"))

probe.db <- readRDS(file.path(param.dir.database, "probe_database.rds"))
TEi.db <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))
probe.genotype.details <- readRDS(file.path(param.dir.bulk, "probe_genotype_details.rds"))


probe.master.dist.db <- readRDS(file.path(param.dir.tmp, "probe_master_dist_db.rds"))
probe.master.dist.db.filtered <- probe.master.dist.db[is.na(probe.master.dist.db$filtered.out), ]
TEi.master.dist.db <- readRDS(file.path(param.dir.tmp, "tei_master_dist_db.rds"))
tei.probe.sum <- probe.master.dist.db.filtered %>% group_by(TEi.id) %>% count(TE) %>% 
  pivot_wider(names_from = TE, values_from = n, values_fill = 0)
names(tei.probe.sum)[names(tei.probe.sum)==1] <- "TE.probes" 
names(tei.probe.sum)[names(tei.probe.sum)==0] <- "ABS.probes" 

merge.tei.seeds.origional <- unique(read.csv(file.path(param.dir.merge,'merge_tei_seeds.csv')))
merge.probe.filter.changes.origional <- unique(read.csv(file.path(param.dir.merge,'merge_probe_filter_changes.csv')))
merge.probe.tei.changes.origional <- unique(read.csv(file.path(param.dir.merge,'merge_probe_tei_changes.csv')))
merge.tei.removed.origional <- unique(read.csv(file.path(param.dir.merge,'merge_tei_removed.csv')))

probe.dupes <- unlist((merge.probe.filter.changes.origional %>% group_by(probe.id) %>% count() %>% filter(n>1))[,1])
for (probe.on in probe.dupes) {
  a <- rownames(merge.probe.filter.changes.origional[merge.probe.filter.changes.origional$probe.id %in% probe.on, ])
  merge.probe.filter.changes.origional <- merge.probe.filter.changes.origional[!rownames(merge.probe.filter.changes.origional) %in% a[2],]
}

merge.probe.filter.changes.origional %>% group_by(probe.id) %>% count() %>% filter(n>1)
merge.probe.tei.changes.origional %>% group_by(probe.id) %>% count() %>% filter(n>1)
tei.dupes <- unlist((merge.tei.removed.origional %>% group_by(TEi.id) %>% count() %>% filter(n>1))[,1])
for (probe.on in probe.dupes) {
  a <- rownames(merge.tei.removed.origional[merge.tei.removed.origional$TEi.id %in% tei.dupes, ])
  merge.tei.removed.origional <- merge.tei.removed.origional[!rownames(merge.tei.removed.origional) %in% a[2],]
}
merge.tei.removed.origional %>% group_by(TEi.id) %>% count() %>% filter(n>1) 



already.modified.probes <- unique(merge.probe.filter.changes.origional$probe.id, 
                                  merge.probe.tei.changes.origional$probe.id)
already.modified.teis <- probe.db[probe.db$probe.id %in% already.modified.probes,]$TEi.id
already.modified.teis <- unique(c(already.modified.teis, merge.tei.seeds.origional$TEi.id, merge.tei.removed.origional$TEi.id))


TEi.pairs <- read.csv(file.path(param.dir.analysis, "NearbyTEis_200_500.csv"))
TEi.pairs <- TEi.pairs[,names(TEi.pairs)[!str_detect(names(TEi.pairs), "^X")]]
TEi.pairs <- left_join(TEi.pairs, tei.probe.sum, by = c("TEi_id1"="TEi.id"))
TEi.pairs <- left_join(TEi.pairs, tei.probe.sum, by = c("TEi_id2"="TEi.id"))
TEi.pairs$min.tei <- apply(TEi.pairs,1,function(X) min(X['TEi_id1'], X['TEi_id2']))
TEi.pairs$max.tei <- apply(TEi.pairs,1,function(X) max(X['TEi_id1'], X['TEi_id2'])) 

TEi.pairs$type <- NA
#TEi.pairs[TEi.pairs$ABS.probes.x==0 & TEi.pairs$ABS.probes.y==0, ]$type <- 'NO_ABS'
#TEi.pairs[TEi.pairs$ABS.probes.x==TEi.pairs$ABS.probes.y & TEi.pairs$TE.probes.x==TEi.pairs$TE.probes.y, ]$type <- 'probe_cnt_match'
#Warning these are the trips!
#TEi.pairs[(TEi.pairs$TEi_id1 %in% c(43909,40140) | TEi.pairs$TEi_id2 %in% c(43909,40140)),]$type <- "TRIP_DO_MANUALLY"
# Removing items with no probes. Probably already merged
if (any(is.na(TEi.pairs$ABS.probes.x))) {
  TEi.pairs[is.na(TEi.pairs$ABS.probes.x), ]$type <- "COMPLETED"  
}
if (any(is.na(TEi.pairs$ABS.probes.y))) {
  TEi.pairs[is.na(TEi.pairs$ABS.probes.y), ]$type <- "COMPLETED"  
}

tmp <- TEi.pairs
tmp <- left_join(tmp, TEi.db[, c("TEi.id", "seed1", "seed1")], by = c("TEi_id1"="TEi.id") )
tmp <- left_join(tmp, TEi.db[, c("TEi.id", "seed1", "seed1")], by = c("TEi_id2"="TEi.id") )
tmp <- add_column(tmp, seed1=NA,seed2=NA,seed3=NA,seed4=NA)
apply_seeds <- function(X) {
  seeds <- sort(as.vector(unlist(unique(c(X['seed1.x'],X['seed1.1.x'],X['seed1.y'],X['seed1.1.y'])))))
  blanks <- 4 - length(seeds)
  for (i in 1:blanks) {
    seeds <- c(seeds,NA)
  }
  seeds
}
tmp.seeds <- apply(tmp, 1, apply_seeds)
tmp[,c('seed1','seed2','seed3','seed4')] <- t(tmp.seeds)
TEi.pairs <- tmp

TEi.pairs.completed <- TEi.pairs[!is.na(TEi.pairs$type) & TEi.pairs$type=="COMPLETED",]
TEi.pairs <- TEi.pairs[is.na(TEi.pairs$type),]

TEi.pairs.merge <- TEi.pairs[str_detect(TEi.pairs$Solution, "^MERGE"),]
TEi.pairs.merge <- TEi.pairs.merge[!TEi.pairs.merge$TEi_id1 %in% already.modified.teis,]
TEi.pairs.merge <- TEi.pairs.merge[!TEi.pairs.merge$TEi_id2 %in% already.modified.teis,]
TEi.pairs.other <- TEi.pairs[!str_detect(TEi.pairs$Solution, "^MERGE"),]
TEi.pairs.other <- TEi.pairs.other[!TEi.pairs.other$TEi_id1 %in% already.modified.teis,]
TEi.pairs.other <- TEi.pairs.other[!TEi.pairs.other$TEi_id2 %in% already.modified.teis,]
TEi.pairs.trip <- TEi.pairs[TEi.pairs$TEi_id1 %in% already.modified.teis | TEi.pairs$TEi_id2 %in% already.modified.teis,]
TEi.pairs.merge.simple.1 <- TEi.pairs.merge[TEi.pairs.merge$Solution %in%  c("MERGE"),]
TEi.pairs.merge.simple.2 <- TEi.pairs.merge[TEi.pairs.merge$Solution %in%  c("MERGE?"),]
TEi.pairs.merge.simple <- rbind(TEi.pairs.merge.simple.1, TEi.pairs.merge.simple.2)
TEi.pairs.merge.cmplx <- TEi.pairs.merge[!TEi.pairs.merge$Solution %in%  c("MERGE","MERGE?"),]

TEi.pairs.remaining <- rbind(TEi.pairs.merge.cmplx, TEi.pairs.other, TEi.pairs.trip)
TEi.pairs.do.nothing <- TEi.pairs.remaining[str_detect(TEi.pairs.remaining$Solution,"K.EP BOTH"), ]
TEi.pairs.remaining <-TEi.pairs.remaining[!str_detect(TEi.pairs.remaining$Solution,"K.EP BOTH"), ]
TEi.pairs.do.nothing.b <- TEi.pairs.remaining[TEi.pairs.remaining$Solution %in% c("?", "?CNV?"), ]
TEi.pairs.do.nothing <- rbind(TEi.pairs.do.nothing, TEi.pairs.do.nothing.b)
rm(TEi.pairs.do.nothing.b)
TEi.pairs.remaining <- TEi.pairs.remaining[!TEi.pairs.remaining$Solution %in% c("?", "?CNV?"), ]
TEi.pairs.remaining.previous.merge <- TEi.pairs.remaining[TEi.pairs.remaining$TEi_id1 %in% already.modified.teis | 
                                                            TEi.pairs.remaining$TEi_id2 %in% already.modified.teis,]

write.csv(TEi.pairs.remaining, file.path("~/Downloads/previous_merge_3f", "merge_3f_summary.csv" ))

for (row.on in 1:nrow(TEi.pairs.remaining)) {
   solutions.a <- TEi.pairs.remaining[row.on, c("TEi_id1", "Solution")]
   solutions.b <- TEi.pairs.remaining[row.on, c("TEi_id2", "Solution")]
   names(solutions.a) <- names(solutions.b) <- c("TEi.id", "Solution")
   solutions <- rbind(solutions.a, solutions.b)
   TEi.tmp <- TEi.db[TEi.db$TEi.id %in% TEi.pairs.remaining[row.on,c("TEi_id1", "TEi_id2")],]
   probe.tmp <- probe.db[probe.db$TEi.id %in% TEi.tmp$TEi.id,]
   probe.tmp <-probe.tmp %>% select(TEi.id, probe.id, filtered.out, probe.SDP.pop2.nearest.int, TE, desc.name, 
                                    side, strand,ref_like_prefix:ref_like_suffix)
   probe.tmp <- left_join(probe.tmp, solutions, by = "TEi.id")
   probe.tmp$previous.merge.tei <- NA
   if (any(probe.tmp$TEi.id %in% already.modified.teis)) {
     probe.tmp[probe.tmp$TEi.id %in% already.modified.teis,]$previous.merge.tei <- TRUE     
   }
   probe.tmp$previous.merge.probe <- NA
   
   if (any(probe.tmp$probe.id %in% already.modified.probes)) {
     probe.tmp[probe.tmp$probe.id %in% already.modified.probes,]$previous.merge.probe <- TRUE
   }
   write.csv(probe.tmp,
             sprintf("~/Downloads/previous_merge_3f/probe_previous_merge_%s.csv", row.on ), row.names = FALSE, na = "")
}

