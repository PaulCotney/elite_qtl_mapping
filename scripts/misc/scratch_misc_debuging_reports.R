#
## Just probes with CNV at probe level
probe.report <- readRDS(file.path(param.dir.database, "probe_database.rds"))
probe.cnv <- probe.report[probe.report$probe.is.CNV, ]
write_probe_report(probe.cnv, "probe_cnv.csv")
#
#
## This is our report of TEis and there probes are in USS and at least one probe doesn't map
probe.report <- readRDS(file.path(param.dir.database, "probe_database.rds"))
TEi.report <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))
TEi.USS <- TEi.report[(!is.na(TEi.report$classification) &
                         TEi.report$classification == "unknown segregating status") &
                        is.na(TEi.report$filtered.out),]$TEi.id
write_probe_report(probe.report[probe.report$TEi.id %in% TEi.USS,], "probes_USS.csv")
write_TEi_report(TEi.report[TEi.report$TEi.id %in% TEi.USS,], "TEi_USS.csv")

TEis.of.probes.not.mapping.to.ELITE <- probe.report[!is.na(probe.report$filter.code) & probe.report$filter.code == 'NOT_MAPPING_TO_ELITE',]$TEi.id
TEis.USS.probe.no.map <- sort(intersect(TEi.USS, TEis.of.probes.not.mapping.to.ELITE))
#
probe.report <- probe.report[probe.report$TEi.id %in% TEis.USS.probe.no.map,]
#
#modifying the precision so it produces a large number when clearly not mapping to ELITE
probe.report[!is.na(probe.report$chr) & probe.report$ELITE.chr != probe.report$chr, ]$mapping.precision <- 1e8
write_probe_report(probe.report, "probe_lod_not_mapping.csv")
#
TEi.report <- TEi.report[TEi.report$TEi.id %in% TEis.USS.probe.no.map ,]
write_TEi_report(TEi.report, 'TEi_lod_not_mapping.csv')
sprintf("Unknown Seg. Status where 1 or more probes don't map to ELITE - TEi.count: %s, probe.count: %s",nrow(TEi.report), nrow(probe.report))



consolidation.report <- read.csv(file.path(param.dir.merge,'consolidation_report.csv'))
teis.care.about <- unique(unlist(consolidation.report[,1:2]))
#
probe.report <- readRDS(file.path(param.dir.database, "probe_database.rds"))
TEi.report <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))
#
write_probe_report(probe.report[probe.report$TEi.id %in% teis.care.about,], "merge_probe_report.csv")
write_TEi_report(TEi.report[TEi.report$TEi.id %in% teis.care.about,], "merge_TEi_report.csv")


TEi.report <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))
report <- TEi.report %>% filter(is.na(filtered.out)) %>%  group_by(my_id, classification) %>% count() %>% pivot_wider(my_id, names_from = classification, values_from =  n)
write.csv(report, file.path(param.dir.output, "Table02.csv"), row.names = FALSE, na = "0")

TEi.report <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))
report <- TEi.report %>% filter(is.na(filtered.out) & str_detect(classification, "segregating")) %>%  group_by(my_id, classification) %>% count() %>% pivot_wider(my_id, names_from = classification, values_from =  n)
write.csv(report, file.path(param.dir.output, "Table02.csv"), row.names = FALSE, na = "")


TEi.db <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))
probe.db <- readRDS(file.path(param.dir.database, "probe_database.rds"))
probe.db[is.na(probe.db$filter.code),]$filter.code <- ""
tei.removed.merged <- TEi.db[TEi.db$filtered.out & TEi.db$filter.code=='MERGED', ]$TEi.id
tei.removed.too.many.probes <- TEi.db[TEi.db$filtered.out & TEi.db$filter.code=='TOO_MANY_PROBES', ]$TEi.id
tei.removed.other.duplicates <- TEi.db[TEi.db$filtered.out & TEi.db$filter.code=='DUPLICATED_TEI', ]$TEi.id
teis.removed <- union(union(tei.removed.merged,tei.removed.too.many.probes),tei.removed.other.duplicates)
probes.removed.for.merge <- probe.db[str_detect(probe.db$filter.code,"MERGE"), ]$probe.id
probes.removed.for.dupe <- probe.db[probe.db$filter.code == "DUPLICATED_PROBE",]$probe.id
probes.removed.for.no.pop2.pop3 <- probe.db[probe.db$filter.code == "NO_POP2_INFORMATION",]$probe.id
probes.removed.cause.of.tei <- probe.db[probe.db$TEi.id %in% teis.removed, ]$probe.id
all.probes.to.remove <- unique(c(probes.removed.for.merge,probes.removed.for.dupe,
                                 probes.removed.for.no.pop2.pop3, probes.removed.cause.of.tei))
probes.to.remove <- data.frame(probe.id = all.probes.to.remove)
probes.to.remove <- add_column(probes.to.remove,TEi_removed = FALSE, merge = FALSE, dupe = FALSE,no.information = FALSE)
probes.to.remove[probes.to.remove$probe.id %in% probes.removed.cause.of.tei,]$TEi_removed <- TRUE
probes.to.remove[probes.to.remove$probe.id %in% probes.removed.for.merge,]$merge <- TRUE
probes.to.remove[probes.to.remove$probe.id %in% probes.removed.for.dupe,]$dupe <- TRUE
probes.to.remove[probes.to.remove$probe.id %in% probes.removed.for.no.pop2.pop3,]$no.information <- TRUE
View(probes.to.remove)
write.csv(probes.to.remove, "~/Downloads/probes_to_remove.csv")
