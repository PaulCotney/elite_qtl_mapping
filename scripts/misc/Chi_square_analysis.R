# Chi_square_analysis ----
## Description:
##
## Author: Paul A Cotney
##
## Date Created: 2024-04-22
##
## Notes:
##
##


## Library ----
library(conflicted)
library(tidyverse)
conflicts_prefer(
  dplyr::filter(),
  dplyr::lag(),
)

## Load directories ----
param.dir.home <- getwd()
param.dir.data <- file.path(param.dir.home,'data')
param.dir.tmp <- file.path(param.dir.home,'tmp')
param.dir.database <- file.path(param.dir.data, "database")
param.dir.bulk <- file.path(param.dir.data,'Bulk')
param.dir.output <- file.path(param.dir.home,"output")
param.dir.annotation <- file.path(param.dir.data,'annotations')
param.dir.data.bulk <- file.path(param.dir.data,"Bulk")

## Load data objects ----
TEi.db <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))

TEi.db <- TEi.db %>% filter(!is.na(segregation.status) & is.na(filtered.out))


TEi.db <- TEi.db %>% mutate(subspecies.expected = dom.correct.flag & cast.correct.flag & mus.correct.flag) 
TEi.db <- TEi.db %>% mutate(subspecies.count = 3 - missing.dom.flag + missing.cast.flag + missing.mus.flag)
TEi.db$subspecies.summary <- "3 subspecies"
TEi.db[TEi.db$subspecies.count != 3, ]$subspecies.summary <- "2 or less subspecies"

TEi.db$subspecies.summary <- factor(TEi.db$subspecies.summary, 
                                    c("3 subspecies", "2 or less subspecies"))
TEi.db$segregation.status.superclass <- TEi.db$segregation.status
TEi.db[str_detect(TEi.db$segregation.status,"Fixed"),]$segregation.status.superclass <- "Fixed"
TEi.db[str_detect(TEi.db$segregation.status,"Segregating"),]$segregation.status.superclass <- "Segregating"
TEi.db <- TEi.db %>% filter(segregation.status.superclass %in% c("Fixed", "Segregating")) %>% select(ERV.type, segregation.status.superclass, subspecies.summary)


results <- data.frame(ERV.type = character(), observed.fixed = character(), observed.segregating = character(),
                      expected.fixed = character(), expected.segregating = character(),
                      residual.fixed = character(), residual.segregating = character(), 
                      chi.squared = numeric(), p.value = numeric())
blank.row <- data.frame(ERV.type = "", observed.fixed = "", observed.segregating = "",
                        expected.fixed = "", expected.segregating = "",
                        residual.fixed = "", residual.segregating = "", 
                        chi.squared = "", p.value = "")
for (erv.type.on in c("MTA","RLTR1IAP","MERVL","RLTR10","ERVB7","RLTRETN","ERVB4","IAPEY3C")) {
  a <- TEi.db %>% filter(ERV.type==erv.type.on & segregation.status.superclass %in% c("Fixed", "Segregating")) %>% 
                   group_by(segregation.status.superclass) %>% count(subspecies.summary) %>% 
                   pivot_wider(names_from = segregation.status.superclass, values_from = n)
  a <- as.data.frame(a)
  rownames(a) <- a[,1]
  a <- a[,-1]
  Xsq <- rstatix::chisq_test(a, correct = FALSE)
  observed <- rstatix::observed_freq(Xsq) 
  expected <- round(rstatix::expected_freq(Xsq),2)
  residual <- round(rstatix::pearson_residuals(Xsq),3)
  new.row <- data.frame(ERV.type = c(erv.type.on,"3 subspecies","2 or less subspecies"), 
                        observed.fixed = c("Observed.fixed", observed[,1]), observed.segregating = c("Observed.segregating", observed[,2]),
                        expected.fixed = c("Expected.fixed", expected[,1]), expected.segregating = c("Observed.segregating", expected[,2]),
                        residual.fixed = c("Residual.fixed", residual[,1]), residual.segregating = c("Residual.segragating", residual[,2]), 
                        chi.squared = c("chi-square", round(Xsq$statistic,2),""), p.value = c("p-value", Xsq$p,""))

  rownames(new.row) <-NULL
  results <- rbind(results,new.row, blank.row)  
}
results <- unname(results)  
write.table(results, file.path(param.dir.output, "chi_square.csv"), row.names = FALSE, col.names = FALSE, sep = ",")


