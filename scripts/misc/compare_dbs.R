# Compare db files (old and new) ----
## Description:
##
## Author: Paul A Cotney
##
## Date Created: 2024-02-14
##
## Notes:
##   
##

## Library ----
library(conflicted)
library(tidyverse)
conflicts_prefer(
  dplyr::filter(),
  dplyr::lag(),
)

## Load directories ----
dir.home <- getwd()
dir.data <- file.path(dir.home, "data", "database")
dir.data.old <- "/Users/paulcotn/Documents/src_R/ELITE_qtl_mapping/data/database_pre_pos_change copy"

## Load probe files ----
old.probe.file <- readRDS(file.path(dir.data.old, "probe_database.rds")) %>% arrange(probe.id)
probe.file <- readRDS(file.path(dir.data, "probe_database.rds")) %>% arrange(probe.id)

## This version has a repeating row for some reason (TEMP) REMOVE
old.probe.file <- old.probe.file %>% unique()

## Clean probe files ----
old.probe.file <- old.probe.file %>% mutate(across(everything(), as.character))
old.probe.file[is.na(old.probe.file)] <- ""
probe.file <- probe.file %>% mutate(across(everything(), as.character))
probe.file[is.na(probe.file)] <- ""

## Check probe files ----
print("Any duplicate rows?")
old.probe.file %>% group_by(probe.id) %>% count() %>% filter(n>1)
probe.file %>% group_by(probe.id) %>% count() %>% filter(n>1)

sprintf("row count matches: %s", nrow(old.probe.file)==nrow(probe.file))

probe.column.changes <- data.frame(col.number = 1:ncol(old.probe.file), old.col.name = names(old.probe.file), new.col.name = names(probe.file))
probe.column.changes$changed <- probe.column.changes$old.col.name != probe.column.changes$new.col.name
print("Any columns change name??")
probe.column.changes[probe.column.changes$changed, ]

sprintf("The probe ids line up: %s",
        all(probe.file[,1:2] == old.probe.file[,1:2]))

results.probe <- NULL
## Run through columns and save values for those that fail---
for (col.on in 1:ncol(probe.file)) {
  sprintf("Scanning column [%s]", probe.column.changes[col.on,1])
  col <- probe.file[,col.on] != old.probe.file[,col.on]
  if (any(col)) {
    new.rows <- data.frame(row.number = which(col), col.number = col.on,
                           old.value = as.character(old.probe.file[col, col.on]), 
                           new.value = as.character(probe.file[col, col.on]))
    if (is.null(results.probe)) {
      results.probe <<- new.rows
    } else {
      results.probe <- rbind(results.probe,new.rows)
    }
    rm(new.rows)
  }
}

report.probe <- inner_join(probe.column.changes[unique(results.probe$col.number),], 
                           results.probe %>% group_by(col.number) %>% count(name = "rows.changed"),
                           by = "col.number") %>% select (-changed)
report.probe

## Load ERV files----
ERV.old.file <- readRDS(file.path(dir.data.old, "ERVi_database.rds"))
ERV.new.file <- readRDS(file.path(dir.data, "ERVi_database.rds"))

## This version has a repeating row for some reason (TEMP) REMOVE
ERV.old.file <- ERV.old.file %>% unique()
ERV.old.file <- ERV.old.file [!row.names(ERV.old.file) %in% c(row.names(ERV.old.file[ERV.old.file$ERVi.id == 24128,])[2]),]

## Clean ERV files ----
ERV.old.file <- ERV.old.file %>% mutate(across(everything(), as.character))
ERV.old.file[is.na(ERV.old.file)] <- ""
ERV.new.file <- ERV.new.file %>% mutate(across(everything(), as.character))
ERV.new.file[is.na(ERV.new.file)] <- ""

## Check ERV files ----
print("Any duplicate rows?")
ERV.old.file %>% group_by(ERVi.id) %>% count() %>% filter(n>1)
ERV.new.file %>% group_by(ERVi.id) %>% count() %>% filter(n>1)

sprintf("row count matches: %s", nrow(ERV.old.file)==nrow(ERV.new.file))

ERV.column.changes <- data.frame(col.number = 1:ncol(ERV.old.file), old.col.name = names(ERV.old.file), new.col.name = names(ERV.new.file))
ERV.column.changes$changed <- ERV.column.changes$old.col.name != ERV.column.changes$new.col.name
print("Any columns change name??")
ERV.column.changes[ERV.column.changes$changed, ]

sprintf("The ERV.ids ids line up: %s",
        all(ERV.new.file[,1] == ERV.old.file[,1]))

results.ERV <- NULL
## Run through columns and save values for those that fail---
for (col.on in 1:ncol(ERV.new.file)) {
  sprintf("Scanning column [%s]", ERV.column.changes[col.on,1])
  col <- ERV.new.file[,col.on] != ERV.old.file[,col.on]
  if (any(col)) {
    new.rows <- data.frame(row.number = which(col), col.number = col.on, 
                           old.value = as.character(ERV.old.file[col, col.on]), 
                           new.value = as.character(ERV.new.file[col, col.on]))
    if (is.null(results.ERV)) {
      results.ERV <<- new.rows
    } else {
      results.ERV <- rbind(results.ERV,new.rows)
    }
    rm(new.rows)
  }
}

ERV.changes.report <- inner_join(ERV.column.changes[unique(results.ERV$col.number),], 
                                 results.ERV %>% group_by(col.number) %>% count(name = "rows.changed"),
                                 by = "col.number") %>% select (-changed)


## Individual analysis goes below as needed ----

###Remove any filtered out rows for the moment as none of those changed.
ERV.rows.filtered.out <- as.integer(row.names(ERV.new.file[ERV.new.file$filtered.out == "TRUE",]))
probe.rows.filtered.out <- as.integer(row.names(probe.file[probe.file$filtered.out == "TRUE",]))


results.ERV.real <- results.ERV %>% filter(!row.number %in% ERV.rows.filtered.out )
inner_join(ERV.column.changes[unique(results.ERV.real$col.number),], 
           results.ERV.real %>% group_by(col.number) %>% count(name = "rows.changed"),
           by = "col.number") %>% select (-changed)
results.probe.real <- results.probe %>% filter(!row.number %in% probe.rows.filtered.out)
inner_join(probe.column.changes[unique(results.probe.real$col.number),], 
           results.probe.real %>% group_by(col.number) %>% count(name = "rows.changed"),
           by = "col.number") %>% select (-changed)

results.ERV.real[results.ERV.real$col.number==22,] %>% group_by(old.value, new.value) %>% count()

segregation.class.changes <- results.ERV.real[results.ERV.real$col.number==12, ]
segregation.class.changes$ERVi.id <- ERV.new.file[segregation.class.changes$row.number,]$ERVi.id
segregation.class.changes <- left_join(segregation.class.changes, ERV.new.file[,c("ERVi.id", "segregation.status")], by = "ERVi.id")
write.csv(segregation.class.changes, "~/tmp/changes.csv", row.names = FALSE, na = "")

row.nums <- results.ERV.real[results.ERV.real$col.number==22,]$row.number
ERV.ids <- ERV.new.file[row.nums,]$ERVi.id
write.csv(ERV.new.file[ERV.new.file$ERVi.id %in% ERV.ids, ], "~/tmp/ERV_example.csv", row.names = FALSE)
write.csv(probe.file[probe.file$ERVi.id %in% ERV.ids, ], "~/tmp/probe_example.csv", row.names = FALSE
          
          )
