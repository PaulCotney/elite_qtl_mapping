# Duplication analysis from Anwica
library(tidyverse)
param.dir.home <- getwd()
param.dir.data <- file.path(param.dir.home,"data")
param.dir.data.ELITE <- file.path(param.dir.data,"ELITE")
param.dir.data.bulk <- file.path(param.dir.data,"Bulk")
param.dir.database <- file.path(file.path(param.dir.data, "database"))
param.dir.annotations <- file.path(param.dir.data,"annotations")
param.dir.tmp <- file.path(param.dir.home,'tmp')
param.dir.output <- file.path(param.dir.home, 'output')
param.dir.output.duplicate <- file.path(param.dir.output, 'duplicate')


TEi.db <- readRDS(file.path(param.dir.database, "ERVi_database.rds"))
probe.db <- readRDS(file.path(param.dir.database, "probe_database.rds"))
duplicates.source <- read.csv(file.path(param.dir.data, "ConsolidateDuplicateTEi.csv"), stringsAsFactors = FALSE)
duplicates.source$duplicate.type <- NA
duplicates.source[str_detect(duplicates.source$note, '^[:digit:]* is a duplicate of [:digit:]*$'),]$duplicate.type <- 'simple duplicate'
duplicates.source[str_detect(duplicates.source$note, '^[:digit:]* is duplicate of [:digit:]*$'),]$duplicate.type <- 'simple duplicate'
duplicates.source[str_detect(duplicates.source$note, 'keep probes from both TEi'),]$duplicate.type <- 'complex.duplicate'


for (row.on in 1:nrow(duplicates.source)) {
  keep.tei <- duplicates.source[row.on,]$keep_TEi
  filter.tei <- duplicates.source[row.on,]$filter_TEi
  TEi.on <- c(keep.tei, filter.tei)
  tei.s <- TEi.db[TEi.db$TEi.id %in% TEi.on,]
  tei.s <- add_column(tei.s, action.plan = NA,.after = "TEi.id")
  tei.s[tei.s$TEi.id == keep.tei,]$action.plan <- 'KEEP'
  tei.s[tei.s$TEi.id == filter.tei,]$action.plan <- 'FILTER OUT'
  names(tei.s)[names(tei.s)=='my_id'] <- "seed1"
  tei.s <- add_column(tei.s,seed2=NA,.after = "seed1")
  out.path <- file.path(param.dir.output.duplicate, "simple")
  if ( duplicates.source[row.on,]$duplicate.type == 'complex.duplicate') {
    out.path <- file.path(param.dir.output.duplicate, "complex")
  }
  
  write.csv(tei.s, file.path(out.path, sprintf("tei_%s_%s.csv", 
                                               keep.tei, filter.tei)), 
            row.names = FALSE, na = "")
  probe.s <- probe.db[probe.db$TEi.id %in% TEi.on,]
  probe.s <- add_column(probe.s, new.tei = NA, .after = "probe.id")
  probe.s <- add_column(probe.s, pac.notes = NA, .after = "new.tei")
  names(probe.s)[names(probe.s)=='my_id'] <- "seed1"
  probe.s <- add_column(probe.s,seed2=NA,.after = "seed1")
  probe.s <-  relocate(probe.s, c("probe.SDP.pop2.nearest.int", "TE", "desc.name"), .after = "seed2")
  probe.s = add_column(probe.s, new.desc.name=NA, .after = "desc.name")
  write.csv(probe.s, file.path(out.path, sprintf("probe_%s_%s.csv", 
                                                                   keep.tei, filter.tei)), 
            row.names = FALSE, na = "")
  
}
