# Header ----
## Description: # Build the final database in the probe_database.rds and ERVi_database.rds
##
## Author: Paul A Cotney
##
## Date Created: 2022-12-17
##
## Notes:
##
##

## Load libraries ----
library(conflicted)
suppressPackageStartupMessages(library(tidyverse))
filter <- dplyr::filter
lag <- dplyr::lag

## Load directories ----
param.dir.home <- getwd()
param.dir.data <- file.path(param.dir.home,'data')
param.dir.tmp <- file.path(param.dir.home,'tmp')
param.dir.bulk <- file.path(param.dir.data,"bulk")
param.dir.elite <- file.path(param.dir.data,"ELITE")
param.dir.external <- file.path(param.dir.data,"External")
param.dir.james <- file.path(param.dir.data,"External_James")
param.dir.annotation <- file.path(param.dir.data,'annotations')
param.dir.misc.meta <- file.path(param.dir.data,'misc_meta')
param.dir.database <- file.path(param.dir.data, "database")
param.dir.config <- file.path(param.dir.home, "config")

## Include functions ----
source(file.path(param.dir.home,'scripts/include_reporting_functions.R'))

## Create and write probe_database.rds ----

### Load probe metadata ----
ELITE.probe.meta <- readRDS(file.path(param.dir.tmp, 'ELITE_probe_table_meta.rds'))
names(ELITE.probe.meta)[names(ELITE.probe.meta) == 'chr'] <- 'ELITE.chr'
names(ELITE.probe.meta)[names(ELITE.probe.meta) == 'pos'] <- 'ELITE.pos'
names(ELITE.probe.meta)[names(ELITE.probe.meta) == 'REF'] <- 'ELITE.REF'
probe.meta <- readRDS(file.path(param.dir.tmp, "probe.meta.rds"))
ELITE.probe.desc <- readRDS(file.path(param.dir.tmp, 'probe_description.rds')) %>% select(TEi.id, probe.id, desc.name)

### Load QTL mapping location ----
probe.mapping <- read.csv(file.path(param.dir.bulk,"top_peaks.csv"), stringsAsFactors = FALSE)
names(probe.mapping)[4:6] <-c('lod', 'interval.distal', 'interval.proximal')

### Load SDP information ----
probe.sdp <- readRDS(file.path(param.dir.tmp, 'probe_sdp_as_String.rds'))

# Getting count of columns that are zero
samples.used <- unlist(read.csv(file.path(param.dir.external, "sample_names_used.csv")))
probe.cc.details <- readRDS(file.path(param.dir.bulk, 'probe_genotype_details.rds'))
probe.cc.details <- probe.cc.details[!is.na(probe.cc.details$final.value) & probe.cc.details$sample %in% samples.used ,]

probe.cc.zero.count <- probe.cc.details %>% select(probe.id, final.value) %>% filter(final.value==0) %>% group_by(probe.id) %>% count()
probe.cc.one.count <- probe.cc.details %>% select(probe.id, final.value) %>% filter(final.value>0) %>% group_by(probe.id) %>% count()
probe.cc.zero.count <- full_join(probe.cc.zero.count, probe.cc.one.count, by = c('probe.id'))
probe.cc.zero.count <- probe.cc.zero.count[,1:2]
names(probe.cc.zero.count)[names(probe.cc.zero.count) == "n.x"] <- "zero.cc.probe.count"
probe.cc.zero.count[is.na(probe.cc.zero.count$zero.cc.probe.count), ]$zero.cc.probe.count <- 0

lambda <- function(X) {
  norms <- X[!is.na(X$final.value),]
  norms <- norms %>% pivot_wider(id_cols = c("probe.id"), names_from = sample, values_from = final.value)
  norms[,-1] <- apply(norms[,-1], c(1,2), round, digits = 2 )
  norms
}

cc.norm.norms <- lambda(probe.cc.details)
sanger.norms <- lambda(readRDS(file.path(param.dir.tmp, 'probe_sanger_founder_details.rds' )))
unc.norms <- lambda(readRDS(file.path(param.dir.tmp, 'probe_unc_founder_details.rds' )))

probe.filter.status <- readRDS(file.path(param.dir.tmp,"probe.annotations.rds")) %>% select(probe.id, remove,CNV,removal.reason,removal.code, manually.marked.as.mapping )
names(probe.filter.status) <- c("probe.id", "filtered.out", "probe.is.CNV", "filter.comments", "filter.code", "manually.marked.as.mapping")
probe.filter.status[!probe.filter.status$probe.is.CNV,]$probe.is.CNV <- NA

probe.copies.by.founder <-  readRDS(file.path(param.dir.database, "probe_genotype_details_raw.rds"))
names(probe.copies.by.founder)[2:9] <- str_replace(names(probe.copies.by.founder)[2:9],'$','.copies')

duplicate.details <- read.csv(file.path(param.dir.annotation, "duplicate_details.csv"), stringsAsFactors = FALSE)

anovoa.pvalues <- read.csv(file.path(param.dir.james, 'anova_p_value.csv'))

new.lod <- read.csv(file.path(param.dir.bulk, 'peak.at.elite.csv'))
names(new.lod)[2] <- 'ELITE.lod'

pop.3.reads <- readRDS(file.path(param.dir.tmp, "pop3_reads.rds"))
names(pop.3.reads)[-1] <- str_replace(names(pop.3.reads)[-1], "(.*)", "pop3.raw.reads.\\1")


probe.db <- left_join(ELITE.probe.meta, ELITE.probe.desc[,-1], by = c("probe.id") )
probe.db <- left_join(probe.db, pop.3.reads, by = c("probe.id") )
probe.db <- left_join(probe.db, duplicate.details[,1:3], by = "probe.id" )
probe.db <- left_join(probe.db, new.lod, by = "probe.id" )
probe.db <- left_join(probe.db, probe.filter.status, by = "probe.id")
probe.db <- left_join(probe.db, anovoa.pvalues, by = c("probe.id") )
probe.db <- left_join(probe.db, probe.meta[,c("probe.id","not.mappable")], by = c("probe.id"))
probe.db[!probe.db$not.mappable,]$not.mappable <- NA
probe.db <- left_join(probe.db, probe.mapping, by = c("probe.id") )
probe.db$mapping.precision <- abs(probe.db$ELITE.pos - probe.db$pos)
probe.db$mapping.interval <- abs(probe.db$interval.distal - probe.db$interval.proximal)
probe.db[!is.na(probe.db$chr) & probe.db$ELITE.chr!= probe.db$chr,]$mapping.precision <- NA

suppressWarnings(probe.db$lod <- as.numeric(probe.db$lod))
probe.db$delta.LOD <- probe.db$lod - probe.db$ELITE.lod

probe.db <- left_join(probe.db, probe.copies.by.founder, by = "probe.id" )
probe.db <- left_join(probe.db, probe.sdp, by = "probe.id" )
probe.db <- left_join(probe.db, probe.cc.zero.count, by = "probe.id" )
probe.db <- left_join(probe.db, sanger.norms, by = "probe.id" )
probe.db <- left_join(probe.db, unc.norms, by = "probe.id" )
probe.db <- left_join(probe.db, cc.norm.norms, by = "probe.id" )

probe.db[is.na(probe.db$probe.is.CNV),]$probe.is.CNV <- !str_detect(probe.db[is.na(probe.db$probe.is.CNV),]$probe.SDP.pop2.final, '^A(1|0|N)*$')
probe.db[!is.na(probe.db$probe.is.CNV) & !probe.db$probe.is.CNV,]$probe.is.CNV <- NA
if (any(!is.na(probe.db$filtered.out) & probe.db$filtered.out != TRUE)) {
  probe.db[!is.na(probe.db$filtered.out) & probe.db$filtered.out != TRUE,]$filtered.out <- NA
}

## Change ELITE.REF to a value depending on if the PROBE is in ref ----
probe.db$ELITE.REF <- FALSE
probe.db[substr(probe.db$probe.SDP.pop2.final,3,3) == "1", ]$ELITE.REF <- TRUE

## Abs probes have NO sides ----
probe.db[probe.db$TE==0,]$side <- ""

### save probe database RDS for the first time----
saveRDS(probe.db, file.path(param.dir.database, "probe_database.rds"))

## Create and write ERVi_database.rds----
# @TODO Place somewhere else in code
# Find if TE probes are on one side or the other only or on both sides
tei.sides.covered <- probe.db %>% filter(is.na(filtered.out)) %>% filter(TE == 1 ) %>% group_by(TEi.id) %>% count(side)
tei.sides.covered$n <- TRUE
tei.sides.covered <- tei.sides.covered %>% pivot_wider(names_from = side, values_from = n, values_fill = FALSE)
lambda <- function(X) {
  if (all(X['end'],X['start'])) {
    "BOTH"
  } else if (X['end']) {
    "END"
  } else if (X['start']) {
    "START"
  }
}
tei.sides.covered$tei.sides.covered <- apply(tei.sides.covered,1, lambda)
tei.sides.covered <- tei.sides.covered[, c("TEi.id", "tei.sides.covered")]
missing.teis <-unique(setdiff(probe.db$TEi.id, tei.sides.covered$TEi.id))
missing <- data.frame(TEi.id = missing.teis, tei.sides.covered = rep(NA,length(missing.teis)) )
tei.sides.covered <- rbind(tei.sides.covered, missing)
#tei.sides.covered %>% group_by(TEi.id) %>% count() %>% filter(n>1)


ELITE.TEi <- readRDS(file.path(param.dir.tmp, "ELITE_TEi_meta.rds"))
names(ELITE.TEi)[names(ELITE.TEi) == 'state'] <- 'ELITE.state'
TEi.classifications <- readRDS(file.path(param.dir.tmp,'tei_classifications.rds'))
TEi.sdp <-  readRDS(file.path(param.dir.tmp, "TEi_sdp_strings.rds"))
TEi.sanger <-  readRDS(file.path(param.dir.tmp, "TE_sanger_zygosity.rds"))
names(TEi.sanger)[names(TEi.sanger)=='SDP'] <- 'TEi.sanger.founder.sdp'
TEi.unc <-  readRDS(file.path(param.dir.tmp, "TE_unc_zygosity.rds"))
names(TEi.unc)[names(TEi.unc)=='SDP'] <- 'TEi.UNC.founder.sdp'
TEi.cc <-  readRDS(file.path(param.dir.tmp, "TE_CC_zygosity.rds"))
subspecies <- read.csv(file.path(param.dir.external, "subspecies.csv"))
TEi.filter.status <- readRDS(file.path(param.dir.tmp, "tei.table.annotations.rds" )) %>% select(-analyst, -remove)
names(TEi.filter.status) <- c("TEi.id", "filter.comments", "filter.code")
TEi.filter.status$filtered.out <- FALSE
TEi.filter.status[TEi.filter.status$filter.code != '',]$filtered.out <- TRUE


TEi.db <- left_join(ELITE.TEi, TEi.sdp, by = c("TEi.id") )
TEi.db <- left_join(TEi.db, TEi.classifications, by = c("TEi.id") )
TEi.db <- left_join(TEi.db, TEi.filter.status, by = c("TEi.id") )
TEi.db <- left_join(TEi.db, subspecies, by = c("TEi.id") )
TEi.db <- left_join(TEi.db, TEi.sanger, by = c("TEi.id") )
TEi.db <- left_join(TEi.db, TEi.unc, by = c("TEi.id") )
TEi.db <- left_join(TEi.db, TEi.cc, by = c("TEi.id") )
TEi.db <- left_join(TEi.db, tei.sides.covered, by = "TEi.id")

TEi.db$is.complete <- !str_detect(TEi.db$completeness.pop2.sdp, '0')
TEi.db$is.cnv <- FALSE
TEi.db$is.cnv.TE.start <- !str_detect(TEi.db$TE.pop2.start.sdp, '^A(1|0|N)*$')
TEi.db$is.cnv.TE.end <- !str_detect(TEi.db$TE.pop2.end.sdp, '^A(1|0|N)*$')
TEi.db$is.cnv.TE <- !str_detect(TEi.db$TE.pop2.sdp, '^A(1|0|N)*$')
TEi.db$is.cnv.ABS <- !str_detect(TEi.db$ABS.pop2.sdp, '^A(1|0|N)*$')
TEi.db$is.cnv.complete <- !str_detect(TEi.db$completeness.pop2.sdp, '^A(1|0|N)*$')
TEi.db$is.cnv.sdp <-  !str_detect(TEi.db$pop2.sdp, '^A(1|0|N)*$')

TEi.db[,c('is.complete','is.cnv','is.cnv.TE.start','is.cnv.TE.end', 'is.cnv.TE','is.cnv.ABS','is.cnv.complete','is.cnv.sdp')] <- apply(
  TEi.db[,c('is.complete','is.cnv','is.cnv.TE.start','is.cnv.TE.end', 'is.cnv.TE','is.cnv.ABS','is.cnv.complete','is.cnv.sdp')], c(1,2),
  function(X) if (is.na(X)) {FALSE} else {X})
TEi.db[TEi.db$is.cnv.TE.start | TEi.db$is.cnv.TE.end |  TEi.db$is.cnv.TE | TEi.db$is.cnv.ABS | TEi.db$is.cnv.complete | TEi.db$is.cnv.sdp,]$is.cnv <- TRUE
TEi.db[,c('is.complete','is.cnv','is.cnv.TE.start','is.cnv.TE.end', 'is.cnv.TE','is.cnv.ABS','is.cnv.complete','is.cnv.sdp')] <- apply(
  TEi.db[,c('is.complete','is.cnv','is.cnv.TE.start','is.cnv.TE.end', 'is.cnv.TE','is.cnv.ABS','is.cnv.complete','is.cnv.sdp')], c(1,2),
  function(X) if (!is.na(X) & X==FALSE) {NA} else {'TRUE'})

### Add in the start 120 and end 120 sequences ----
seq120 <- read.csv(file.path(param.dir.elite, "ERV120.csv"))
TEi.db <- left_join(TEi.db, seq120, by=c("TEi.id"="ERVi.id"))

#### Update ERV information in the probe db ----
# probe.db <- readRDS(file.path(param.dir.database, "probe_database.rds"))
# probe.db <- left_join(probe.db, TEi.db[,c("TEi.id", "chr", "pos")], by = "TEi.id")
# probe.db[,c("ELITE.chr", "ELITE.pos")] <- probe.db[,c("chr.y", "pos.y")]
# probe.db <- probe.db[,!names(probe.db) %in% c("chr.y", "pos.y")]
# names(probe.db) <- str_replace_all(names(probe.db),"^(.*)\\.x", "\\1")
# probe.db <- probe.db %>% arrange("ELITE.chr", "ELITE.pos", "TEi.id", "probe.id")


### Change column names for the probe db ----
probe.db.col.names <- read.csv(file.path(param.dir.config, "probe_db_col_names.csv"))
probe.db.col.names <- probe.db.col.names[nchar(trimws(probe.db.col.names$new_name))!=0,]
for (row.on in 1:nrow(probe.db.col.names)) {
  names(probe.db)[names(probe.db)==probe.db.col.names[row.on,]$old_name] <-
    probe.db.col.names[row.on,]$new_name
}

### Change 1/0 to boolean for certain columns ----
probe.db$ERV.in.reference <- as.logical(probe.db$ERV.in.reference)
probe.db$is.TE.probe <- as.logical(probe.db$is.TE.probe)

### Set columns that have Blanks in a boolean as FALSE ----
probe.db[is.na(probe.db$manually.marked.as.mapping),]$manually.marked.as.mapping <- FALSE
probe.db$manually.marked.as.mapping <- as.logical(probe.db$manually.marked.as.mapping)

#### Resave the probe db ----
saveRDS(probe.db, file.path(param.dir.database, "probe_database.rds"))

### Change column names for the ERV db ----
ERV.db.col.names <- read.csv(file.path(param.dir.config, "erv_db_col_names.csv"))

##### Remove flagged cols ----
cols.to.remove <- ERV.db.col.names[ERV.db.col.names$removed,]$old_name
TEi.db <- TEi.db[,names(TEi.db)[!names(TEi.db) %in% cols.to.remove]]

#### Change booleans to true booleans for the database
b.cols <- c('is.complete', 'is.cnv', 'is.cnv.TE.start', 'is.cnv.TE.end', 'is.cnv.TE', 'is.cnv.ABS')
TEi.db[,b.cols][is.na(TEi.db[,b.cols])] <- FALSE
TEi.db[,b.cols] <- apply(TEi.db[,b.cols],c(1,2), as.logical)

##### Rename column names ----
# Ignore rows where the name doesn't change
ERV.db.col.names <- ERV.db.col.names[nchar(trimws(ERV.db.col.names$new_name))!=0,]

for (row.on in 1:nrow(ERV.db.col.names)) {
  names(TEi.db)[names(TEi.db)==ERV.db.col.names[row.on,]$old_name] <-
    ERV.db.col.names[row.on,]$new_name
}

### Save the TEi db to RDS file ----
TEi.db <- TEi.db %>% arrange("chr", "pos", "TEi.id")
saveRDS(TEi.db, file.path(param.dir.database, "ERVi_database.rds"))

